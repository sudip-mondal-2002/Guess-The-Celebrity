package com.sudip.guessthecelebrity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private String sourceUrl = "https://www.therichest.com/top-lists/top-100-richest-celebrities/";
    ArrayList<String> celebArray = new ArrayList<String>();
    ArrayList<String> celebNames = new ArrayList<String>();
    private int chosenCeleb;
    ImageView imageView;
    ArrayList<Integer> options = new ArrayList<Integer>();

    public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);

                return bitmapImage;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    public void createArraysForCelebs() {
        DownloadTask task = new DownloadTask();
        String result = null;
        try {
            result = task.execute(sourceUrl).get();

            Pattern p = Pattern.compile("source media=\"(min-width: 0px)\" sizes=\"60px\" data-srcset=\"(.*?)q=");
            Matcher m = p.matcher(result);
            while (m.find()) {
                celebArray.add(m.group(1));
            }
            p = Pattern.compile("<img class=\"lazyload\" alt=\"\" />\n" +
                    "                        </picture>\n" +
                    "    \n" +
                    "</div>\n" +
                    "</div>\n" +
                    "                                       \n" +
                    "                                </div>\n" +
                    "                                (.*?)\n" +
                    "                            </a>");
            m = p.matcher(result);
            while (m.find()) {
                celebNames.add(m.group(1));
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void randomCelebGenerator() {
        Random random = new Random();
        chosenCeleb = random.nextInt(celebArray.size());
    }

    public void setImage() {
        imageView = (ImageView) findViewById(R.id.imageView);

        Bitmap celebImage;
        DownloadImage imageDownloader = new DownloadImage();
        try {
            celebImage = imageDownloader.execute(celebArray.get(chosenCeleb)).get();
            imageView.setImageBitmap(celebImage);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setOptions() {
        Random random = new Random();
        Integer correctOption = random.nextInt(4);
        options.set(correctOption, (chosenCeleb));
        for (Integer i = 0; i < 4; i++) {
            if (i != correctOption) {
                Integer nextOption = random.nextInt(celebArray.size());
                while ((options.get(0) == nextOption) || (options.get(1) == nextOption) || (options.get(2) == nextOption) || (options.get(3) == nextOption)) {
                    nextOption = random.nextInt(celebArray.size());
                }
                options.set(i, nextOption);
            }

        }
    }

    public void setOptionsToApp() {
        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button4);
        button1.setText(celebNames.get(options.get(0)));
        button2.setText(celebNames.get(options.get(1)));
        button3.setText(celebNames.get(options.get(2)));
        button4.setText(celebNames.get(options.get(3)));
    }

    public void setup() {
        randomCelebGenerator();
        options.add(celebArray.size());
        options.add(celebArray.size());
        options.add(celebArray.size());
        options.add(celebArray.size());
        setImage();
        setOptions();
        setOptionsToApp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createArraysForCelebs();
        setup();
    }

    public void onClickHandler(View view)
    {
        Button button = (Button) view;
        String text;
        if (button.getText() == celebNames.get(chosenCeleb))
        {
            text = "Correct Answer";
        }
        else
        {
            text = "Wrong Answer";
        }
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
        toast.show();
        setup();
    }
}